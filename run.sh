#!/bin/bash
## ./run.sh production ## run on server
## ./run.sh ## defaults to local settings
## IMPORTANT: must have http-server installed: `npm install -g http-server`

PORT=2016 ## set desired port number here

if [ "$1" == "production" ]; then
	http-server -p $PORT ## map to 0.0.0.0 if running from server
else
	http-server -a localhost -p $PORT ## map to localhost
fi

echo "closing..."
